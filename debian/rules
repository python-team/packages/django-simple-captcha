#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE = 1

include /usr/share/dpkg/pkg-info.mk

BUILD_DATE  = $(shell LC_ALL=C date -u "+%d %B %Y" -d "@$(SOURCE_DATE_EPOCH)")
SPHINXOPTS := -E -N -D html_last_updated_fmt="$(BUILD_DATE)"

export PYBUILD_NAME=django-captcha

%:
	dh $@ --buildsystem=pybuild

override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. python3 -m sphinx -b html $(SPHINXOPTS) docs/ $(CURDIR)/debian/python-django-captcha-doc/usr/share/doc/python-django-captcha-doc/html
	dh_sphinxdoc
endif

.PHONY: override_dh_auto_test
override_dh_auto_test:
	dh_auto_test -- --system=custom --test-args="cd testproject && {interpreter} manage.py test captcha"

override_dh_install:
	dh_install
	find debian/python3-django-captcha -name Vera.ttf -exec \
		ln -sf /usr/share/fonts/truetype/ttf-bitstream-vera/Vera.ttf {} \;;
	# Remove these *.TXT files, we link to the used font any way.
	rm debian/python3-django-captcha/usr/lib/python3*/dist-packages/captcha/fonts/*.TXT

override_dh_auto_build: rebuild-locale
	dh_auto_build

rebuild-locale:
	@echo
	@echo "Rebuild the GNU message catalogs..."
	@echo
	for lang in `find captcha/locale/ -maxdepth 1 -type d | sort`; do \
	    if [ -f $${lang}/LC_MESSAGES/django.po ]; then \
	        LANG=`basename $${lang}` ;\
	        echo "Create message catalog for '$${LANG}'" ;\
	        msgfmt -v $${lang}/LC_MESSAGES/django.po -o $${lang}/LC_MESSAGES/django.mo ;\
	    fi ;\
	done
